// Initialize your app
var myApp = new Framework7({
  animateNavBackIcon:true,
  template7Pages: true,
  precompileTemplates: true,
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
    // Enable Dom Cache so we can use all inline pages
    domCache: true
  });


mainView.hideNavbar();

$$(document).on('page:init', function (e) {
  var page = e.detail.page;
    // Code for Register page
    if (page.name === 'register') {
     // $$('#nv').removeClass('navbar-hidden'); 
     mainView.showNavbar();
   }  
 });

$$(document).on('page:reinit', function (e) {
  var page = e.detail.page;
    // Code for Register page
    if (page.name === 'register') {
     // $$('#nv').removeClass('navbar-hidden'); 
     mainView.showNavbar();
   }
 
  });

$$(document).on('page:back', function (e) {
  // Do something here when page loaded and initialized
  mainView.hideNavbar();
});


// Funcion to handle Cancel button on Login page
$$('#cancel-login').on('click', function () {
  // Clear field values
  $$('#login-email').val('');
  $$('#login-password').val('');
});

// Funcion to handle Submit button on Login page
$$('#submmit-login').on('click', function () { 
  var i = $$('#login-identitas').val();
  var u = $$('#login-username').val();
  var p = $$('#login-password').val(); 
  var query = 'http://duitkas.com/api/login'; 
  myApp.showIndicator();
  $$.post(query, {identitas:i,email:u, pass:p}, function (data) {  
    data = JSON.parse(data); 
    myApp.hideIndicator();
    if(data.error == 0){
      localStorage.id_perusahaan = data.id_perusahaan;
      localStorage.id_user = data.id_user;
      localStorage.nama = data.username;
      localStorage.identitas = data.identitas;
      localStorage.role = data.role;
      // mainView.router.loadPage('dashboard.html');  
      window.location = "dashboard.html"; 
    }
    else
      myApp.alert('Identitas, Username atau Password yang anda masukkan tidak valid!','');
  });

});


// Function to handle Submit button on Register page
$$('#submmit-register').on('click', function () { 
  var i = $$('#register-identitas').val();
  var u = $$('#register-username').val();
  var p = $$('#register-password').val(); 
  var n = $$('#register-nama').val();
  var e = $$('#register-email').val();
  var t = $$('#register-telp').val(); 
  var k = $$('#register-promo').val();  
  var query = 'http://duitkas.com/api/register';
  var mydata = {  identitas:i,
    email:e, 
    pass:p, 
    nama:n,
    username:u, 
    telp:t,
    promo:k,
  }; 
  myApp.showIndicator();
  $$.post(query, mydata , function (data) {  
    data = JSON.parse(data); 
    myApp.hideIndicator();
    if(data.status == 'true'){
      myApp.alert('Register berhasil.','');
      mainView.router.back();
    } else
    myApp.alert('Register gagal. Silahkan coba kembali.','');
  });

});