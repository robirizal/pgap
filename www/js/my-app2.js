// Initialize your app
var d = new Date();
var n = d.getFullYear();
var thn_msk = d.getFullYear(); //tahun page pemasukan
var thn_klr = d.getFullYear(); //tahun page pengeluaran
var thn_lb = d.getFullYear(); //tahun page laporan bulanan
var thn_lt = d.getFullYear(); //tahun page laporan tahunan
var m = d.getMonth() + 1;
var m_msk = d.getMonth() + 1; //bulan page pemasukan
var m_klr = d.getMonth() + 1; //bulan page pengeluaran
var m_lb = d.getMonth() + 1; //bulan page laporan bulanan
var o = d.getDate(); 
if(m < 10) m = '0'+m;
if(m_klr < 10) m_klr = '0'+m_klr;
if(m_msk < 10) m_msk = '0'+m_msk;
if(m_lb < 10) m_lb = '0'+m_lb;
if(o < 10) o = '0'+o;
var today = n+'-'+m+'-'+o;
var url_link = "http://duitkas.com/api/";
var query_user = url_link+'user'; 
var query_pemasukan = url_link+'pemasukan'; 
var query_pengeluaran = url_link+'pengeluaran'; 
var query_tambah_pemasukan = url_link+'pemasukan/getKategori'; 
var query_k_pengeluaran = url_link+'pengeluaran/getKategori'; 
var query_k_relevansi = url_link+'kategori/getRelevansi'; 
var query_kategori = url_link+'kategori'; 
var query_hutang = url_link+'hutang'; 
var query_arsip = url_link+'hutang/getArsip'; 
var query_detail_arsip = url_link+'hutang/getDetailArsip'; 
var query_cicilan = url_link+'hutang/getCicilan'; 
var query_perencanaan = url_link+'perencanaan'; 
var query_lap_tahun = url_link+'laporan/tahunan'; 
var query_lap_bulan = url_link+'laporan/bulanan'; 
// var query_dl_bulan = url_link+'excel_api/bulanan'; 
var i = localStorage.id_perusahaan;
var myrole = localStorage.role;
var u = localStorage.id_user;
var myApp = new Framework7({
  init: false, //untuk data dashboard
  animateNavBackIcon:true,
  template7Pages: true,
  precompileTemplates: true,
  preprocess: function (content, url, next) { 
    switch(url){
      case 'user.html':    
        $$.post(query_user, {id_perusahaan:i,role:myrole,id_user:u}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'perencanaan.html':    
        $$.post(query_perencanaan, {id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'ubah-perencanaan.html':    
        $$.post(query_perencanaan, {id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'kategori.html':    
        $$.post(query_kategori, {id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'hutang.html':    
        $$.post(query_hutang, {id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'arsip-hutang.html':    
        $$.post(query_arsip, {id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'detail-arsip.html':    
        $$.post(query_detail_arsip, {id_hutang:localStorage.id_hutang}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'cicilan-hutang.html':    
        $$.post(query_cicilan, {id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'pemasukan.html':
        var mydata = {id_perusahaan:i,filter:m_msk,tahun:thn_msk};
        // // console.log(n+'-'+m);
        $$.post(query_pemasukan, mydata , function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
            next(compiled(dataJSON)); 
        });
      break;
      case 'pengeluaran.html':
        var mydata = {id_perusahaan:i,filter:m_klr,tahun:thn_klr};
        // console.log("bln = "+m_klr+" thn = "+thn_klr);
        $$.post(query_pengeluaran, mydata , function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          // console.log(dataJSON);
          next(compiled(dataJSON)); 
        });
      break;
      case 'lap-tahunan.html':
        var mydata = {id:i,tahun:thn_lt};
        $$.post(query_lap_tahun, mydata , function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'lap-bulanan.html':
        var mydata = {id:i,bulan:m_lb,tahun:thn_lb};
        $$.post(query_lap_bulan, mydata , function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data); 
          next(compiled(dataJSON)); 
        });
      break;
      case 'ubah-pemasukan.html':  
        $$.post(query_tambah_pemasukan,{id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
          next(compiled(dataJSON)); 
        });
      break;
      case 'ubah-pengeluaran.html':  
        $$.post(query_k_pengeluaran,{id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
          next(compiled(dataJSON)); 
        });
      break;
      case 'ubah-kategori.html':  
        $$.post(query_k_relevansi,{id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
          next(compiled(dataJSON)); 
        });
      break;
      case 'tambah-pemasukan.html':  
        $$.post(query_tambah_pemasukan,{id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
          next(compiled(dataJSON)); 
        });
      break;
      case 'tambah-pengeluaran.html':  
        $$.post(query_k_pengeluaran,{id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
          next(compiled(dataJSON)); 
        });
      break;
      case 'tambah-kategori.html':  
        $$.post(query_k_relevansi,{id_perusahaan:i}, function (data) {  
          var compiled = Template7.compile(content);
          var dataJSON = JSON.parse(data);
          next(compiled(dataJSON)); 
        });
      break;
      default:
        $$('span#nama-user').html(localStorage.nama); 
        var template = Template7.compile(content);
        var resultContent = template();
        return resultContent; 
      break;

    }
  }
});

// Export selectors engine
var $$ = Dom7;

// Add main View
var mainView = myApp.addView('.view-main', {
    // Enable dynamic Navbar
    dynamicNavbar: true,
    // Enable Dom Cache so we can use all inline pages
    domCache: true
}); 

function getWarna(hex){
 switch(hex){
   case '#5ac8fa':    
   return "Light Blue";
   break;
   case '#ffcc00':    
   return "Yellow";
   break;
   case '#ff9500':    
   return "Orange";
   break;
   case '#ff2d55':    
   return "Pink";
   break;
   case '#007aff':    
   return "Blue";
   break;
   case '#4cd964':    
   return "Green";
   break;
   case '#ff3b30':    
   return "Red";
   break;
   case '#e91e63':    
   return "Pink";
   break;
   case '#9c27b0':    
   return "Purple";
   break;
   case '#673ab7':    
   return "Deep Purple";
   break;
   case '#3f51b5':    
   return "Indigo";
   break;
   case '#2196f3':    
   return "Blue";
   break;
   case '#00bcd4':    
   return "Cyan";
   break;
   case '#009688':    
   return "Teal";
   break;
   case '#4caf50':    
   return "Green";
   break;
   case '#8bc34a':    
   return "Light Green";
   break;
   case '#cddc39':    
   return "Lime";
   break;
   case '#ffeb3b':    
   return "Yellow";
   break;
   case '#ffc107':    
   return "Amber";
   break;
   case '#ff9800':    
   return "Orange";
   break;
   case '#ff5722':    
   return "Deep Orange";
   break;
   case '#795548':    
   return "Brown";
   break;
   case '#9e9e9e':    
   return "Gray";
   break;
   case '#607d8b':    
   return "Blue Gray";
   break;
   default:
   return "-"; 
   break;
 }
}

//ganti nama user yang login
$$('span#nama-user').html(localStorage.nama); 

//ganti tahun copyright footer
document.getElementById("copyright").innerHTML = d.getFullYear();

/* Menu Handlers */
$$(document).on('click', '#dashboard', function (e) { 
  // myApp.closePanel(); 
  // mainView.router.loadPage('dashboard.html');
  // $$('#nama-user').html(localStorage.nama);
   window.location = "dashboard.html";
}); 

$$(document).on('click', '#cicilan', function (e) { 
  myApp.closePanel(); 
  mainView.router.loadPage({  url:  'cicilan-hutang.html',
                                      force: true,
                                      ignoreCache: true
                                    });
}); 

$$(document).on('click', '#logout', function (e) {
  window.location = "index.html"; 
});



$$(document).on('page:init', function (e) {
  var page = e.detail.page;
    //------------------ KONFIGURASI ---------------//
    if (page.name === 'konfigurasi') { 
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel 
      $$('span#nama-user').html(localStorage.nama);  
      var i = localStorage.id_perusahaan; 
      var query = url_link+'konfigurasi'; 
      $$.post(query, {id_perusahaan:i}, function (data) {  
        data = JSON.parse(data); 
        var str = data[0].identitas;
        var identitas = str.replace(".duitkas.com", "");
        if(data[0].status == 'Aktif'){ 
          $$('input#konfig-identitas').val(identitas); 
          $$('input#konfig-nama').val(data[0].nama_perusahaan); 
          $$('input#konfig-direktur').val(data[0].nama_direktur); 
          $$('input#konfig-kota').val(data[0].kota); 
          $$('input#konfig-provinsi').val(data[0].provinsi); 
          $$('input#konfig-kontak').val(data[0].kontak); 
          $$('input#konfig-jenis').val(data[0].jenis_usaha); 
          $$('textarea#konfig-alamat').val(data[0].alamat);   
        }
        else
          myApp.alert('Id Perusahaan tidak valid!','');
      }); 

      $$('#submmit-konfig').on('click', function () { 
        var i = $$('#konfig-identitas').val();
        var n = $$('#konfig-nama').val();
        var d = $$('#konfig-direktur').val(); 
        var k = $$('#konfig-kota').val();
        var p = $$('#konfig-provinsi').val();
        var a = $$('#konfig-alamat').val(); 
        var j = $$('#konfig-jenis').val();  
        var t = $$('#konfig-kontak').val();  
        var query = url_link+'konfigurasi/update';
        var mydata = {  id:localStorage.id_perusahaan,
                        identitas:i,
                        nama:n, 
                        direktur:d, 
                        kota:k,
                        provinsi:p, 
                        alamat:a,
                        jenis:j,
                        kontak:t
                      };
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Update data berhasil.',''); 
          } else
          myApp.alert('Update data gagal. Silahkan coba kembali.','');
        }); 
      });
    }  

    //------------ DASHBOARD ------------//
    if(page.name === 'dashboard'){
      var myrole = localStorage.role;
      if(myrole === "2"){
        $$("li#tree-perencanaan").hide();
        $$("li#tree-kategori").hide();
        $$("li#tree-konfigurasi").hide();
        $$("li#tree-user").hide();
      }
      $$('#acc-item-hutang').once('click', function (e) {  
        myApp.accordionClose("#acc-item-lap");
      });
      var month = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var bln = month[d.getMonth()];
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel
      $$('span#nama-user').html(localStorage.nama); 
      var i = localStorage.id_perusahaan;
      var mydata = {id:i};
      var query = url_link+'dashboard/getPemasukan'; 
      $$.post(query, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON.status;
          if(status === 'true') {
            $$('div#pemasukan').html('Rp '+dataJSON.jumlah); 
          }
      });
      var query = url_link+'dashboard/getPengeluaran'; 
      $$.post(query, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON.status;
          if(status === 'true') {
            $$('div#pengeluaran').html('Rp '+dataJSON.jumlah); 
          }
      });
      var query = url_link+'dashboard/getBalance'; 
      $$.post(query, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON.status;
          if(status === 'true') {
            $$('div#balance').html('Rp '+dataJSON.jumlah); 
          }
      });
      var query = url_link+'dashboard/getHutang'; 
      $$.post(query, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON.status;
          if(status === 'true') {
            $$('h5#hutang').html('Rp '+dataJSON.jumlah); 
          }
      });
      var query = url_link+'dashboard/getPiutang'; 
      $$.post(query, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON.status;
          if(status === 'true') {
            $$('h5#piutang').html('Rp '+dataJSON.jumlah); 
          }
      });
      var query = url_link+'dashboard/getKas'; 
      $$.post(query, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON.status;
          if(status === 'true') {
            $$('h5#kas').html('Rp '+dataJSON.jumlah); 
          }
      });
      $$('.bulan').html(bln); 
      $$('.tahun').html(d.getFullYear()); 
      
      //----- CHART PEMASUKAN VS PENGELUARAN -----//
      var query = url_link+'dashboard/getTotalChart'; 
      $$.post(query, mydata , function (data) { 
          var dataJSON = JSON.parse(data);
          var pemasukan = dataJSON['pemasukan'];
          var pengeluaran = dataJSON['pengeluaran'];
          // UNTUK CHART //
          var barData = {
                    labels: [
                    'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
                    ],
                    datasets: [
                    {
                        label: "Grafik Pemasukan",
                        fillColor: "#77BBFF",
                        strokeColor: "#77BBFF",
                        pointColor: "#77BBFF",
                        pointStrokeColor: "#77BBFF",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#77BBFF",
                        data: [
                        pemasukan[0],pemasukan[1],pemasukan[2],pemasukan[3],
                        pemasukan[4],pemasukan[5],pemasukan[6],pemasukan[7],
                        pemasukan[8],pemasukan[9],pemasukan[10],pemasukan[11]
                        ]
                    },
                    {
                        label: "Grafik Pengeluaran",
                        fillColor: "#33577B",
                        strokeColor: "#33577B",
                        pointColor: "#33577B",
                        pointStrokeColor: "#33577B",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "#33577B",
                        data: [
                        pengeluaran[0],pengeluaran[1],pengeluaran[2],pengeluaran[3],
                        pengeluaran[4],pengeluaran[5],pengeluaran[6],pengeluaran[7],
                        pengeluaran[8],pengeluaran[9],pengeluaran[10],pengeluaran[11]
                        ]
                    },
                    ]
                };


                var context = document.getElementById('chartdiv').getContext('2d');
                var myChart = new Chart(context).Line(barData, {
                    scaleLabel : "<%= parseInt(value).toLocaleString(undefined) %>",
                    scaleOverride:false,
                    scaleSteps:5,
                    scaleStartValue:0,
                    scaleStepWidth:250000000,
                    showScale: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: false,
                    //String - Colour of the grid lines
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - Whether the line is curved between points
                    bezierCurve: true,
                    //Number - Tension of the bezier curve between points
                    bezierCurveTension: 0.3,
                    //Boolean - Whether to show a dot for each point
                    pointDot: false,
                    //Number - Radius of each point dot in pixels
                    pointDotRadius: 4,
                    //Number - Pixel width of point dot stroke
                    pointDotStrokeWidth: 1,
                    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                    pointHitDetectionRadius: 20,
                    //Boolean - Whether to show a stroke for datasets
                    datasetStroke: true,
                    //Number - Pixel width of dataset stroke
                    datasetStrokeWidth: 2,
                    //Boolean - Whether to fill the dataset with a color
                    datasetFill: false, //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                    maintainAspectRatio: true,
                    //Boolean - whether to make the chart responsive to window resizing
                    responsive: true
        });
        document.getElementById('js-legend').innerHTML = myChart.generateLegend();
        //-----------------------------------------------------------//
      });

     //----- CHART PENGELUARAN VS PERENCANAAN -----//
      var query = url_link+'dashboard/getSecondChart'; 
      $$.post(query, mydata , function (data) { 
          var dataJSON = JSON.parse(data);
          var perencanaan = dataJSON['perencanaan'];
          var pengeluaran = dataJSON['pengeluaran'];
          var label = dataJSON['label'];
          // UNTUK CHART //
          var barData = {
            labels: label ,
            datasets: [
            {
              label: "Grafik Perencanaan",
              fillColor: "#ff8c1a",
              strokeColor: "#ff8c1a",
              pointColor: "#ff8c1a",
              pointStrokeColor: "#ff8c1a",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "#ff8c1a",
              data: perencanaan
            },
            {
              label: "Grafik Pengeluaran",
              fillColor: "#33577B",
              strokeColor: "#33577B",
              pointColor: "#33577B",
              pointStrokeColor: "#33577B",
              pointHighlightFill: "#fff",
              pointHighlightStroke: "#33577B",
              data: pengeluaran
            },
            ]
          };


          var context = document.getElementById('chartdiv2').getContext('2d');
          var myChart = new Chart(context).Line(barData, {
            scaleLabel : "<%= parseInt(value).toLocaleString(undefined) %>",
            scaleOverride:false,
            scaleSteps:5,
            scaleStartValue:0,
            scaleStepWidth:250000000,
            showScale: true,
            //Boolean - Whether grid lines are shown across the chart
            scaleShowGridLines: false,
            //String - Colour of the grid lines
            scaleGridLineColor: "rgba(0,0,0,.05)",
            //Number - Width of the grid lines
            scaleGridLineWidth: 1,
            //Boolean - Whether to show horizontal lines (except X axis)
            scaleShowHorizontalLines: true,
            //Boolean - Whether to show vertical lines (except Y axis)
            scaleShowVerticalLines: true,
            //Boolean - Whether the line is curved between points
            bezierCurve: true,
            //Number - Tension of the bezier curve between points
            bezierCurveTension: 0.3,
            //Boolean - Whether to show a dot for each point
            pointDot: false,
            //Number - Radius of each point dot in pixels
            pointDotRadius: 4,
            //Number - Pixel width of point dot stroke
            pointDotStrokeWidth: 1,
            //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
            pointHitDetectionRadius: 20,
            //Boolean - Whether to show a stroke for datasets
            datasetStroke: true,
            //Number - Pixel width of dataset stroke
            datasetStrokeWidth: 2,
            //Boolean - Whether to fill the dataset with a color
            datasetFill: false, //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
            maintainAspectRatio: true,
            //Boolean - whether to make the chart responsive to window resizing
            responsive: true
          });
          document.getElementById('js-legend-3').innerHTML = myChart.generateLegend();
        //-----------------------
      });
          
                                                                                  
    }

    //------------ LAPORAN TAHUNAN ------------//
    if(page.name === 'lap-tahunan'){
      $$('span#nama-user').html(localStorage.nama); 
      var arr_thn = [];
      for (var i = 2016; i <= d.getFullYear(); i++) { arr_thn.push(i); }
      var thn = arr_thn.indexOf(parseInt(thn_lt));
      var target = $$(page.container).find('#picker-tahun');
        var picker = myApp.picker({
          input: target,
          rotateEffect: true,
          cols: [
              {
                  textAlign: 'left',
                  values: (function () {
                                          var arr = [];
                                          for (var i = 2016; i <= d.getFullYear(); i++) { arr.push(i); } //n = tahun sekarang
                                          return arr;
                                      })(),
              },
          ],
          onOpen: function (picker) {
       
            var col0Values = picker.cols[0].values;
            var col0Random = col0Values[thn];
 
            picker.setValue([col0Random]);
        
         }
      }); 
      picker.setValue([thn_lt]);
      $$('#filter-tahun').on('click', function(){
        var i = localStorage.id_perusahaan;
        var dt = $$('#picker-tahun').val(); 
        var mydata = {id:i,tahun:dt};
        myApp.showIndicator();
        $$.post(query_lap_tahun, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON[0].status;
          // if(status === 'true') {
            myApp.hideIndicator();
            thn_lt = dt; //ganti data tahun untuk proses preroute
            mainView.router.refreshPage();
          // }
        });
        
      });
    }

     //------------ LAPORAN BULANAN ------------//
    if(page.name === 'lap-bulanan'){
//       $$('#download-bulanan').on('click', function () {
//        // myApp.alert('DL clicked : '+n+'-'+m,'');
//        var i = localStorage.id_perusahaan;
//        var mydata = {id_perusahaan:i,bulan:m,tahun:n};
//         myApp.showIndicator();
//         $$.post(query_dl_bulan, mydata , function (data) {   
         
// myApp.hideIndicator();
//             mainView.router.refreshPage();
//           // var dataJSON = JSON.parse(data);
//           // var status = dataJSON.status;
//           // if(status === 'true') {
//           //   myApp.hideIndicator();
//           //   mainView.router.refreshPage();
//           // }
//         });
//       });
      var month = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var bln = month[m_lb-1]; 
      var a = month.indexOf(bln);
      var arr_thn = [];
      for (var i = 2016; i <= d.getFullYear(); i++) { arr_thn.push(i); }
      var thn = arr_thn.indexOf(parseInt(thn_lb));
      $$('span#nama-user').html(localStorage.nama); 
      var target = $$(page.container).find('#picker-filter');
      var pickerLapBul = myApp.picker({
          input: target,
          rotateEffect: true,
          formatValue: function (p, values, displayValues) {
              return  values[0]+ '-' + values[1] ;
          },
          cols: [
              {
                  textAlign: 'left',
                  values: (function () {
                                          var arr = [];
                                          for (var i = 2016; i <= d.getFullYear(); i++) { arr.push(i); } //n = tahun sekarang
                                          return arr;
                                      })(),
              },
              { 
                  values: ('Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember').split(' '),
              },
          ],
          onOpen: function (picker) {
       
            var col0Values = picker.cols[0].values;
            var col0Random = col0Values[thn];
 
            var col1Values = picker.cols[1].values;
            var col1Random = col1Values[a];
 
            picker.setValue([col0Random, col1Random]);
        
         }
      }); 
      pickerLapBul.setValue([thn_lb,bln]);
      $$('#filter').on('click', function(){
        var i = localStorage.id_perusahaan;
        var dt = $$('#picker-filter').val(); 
        var th = dt.slice(0, 4);
        var bulan = dt.slice(5);
        var a = month.indexOf(bulan)+1;
        if(a < 10) a = '0'+a;
         var mydata = {id:i,bulan:a,tahun:th};
         myApp.showIndicator();
        $$.post(query_lap_bulan, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON[0].status;
          // if(status === 'true') {
            myApp.hideIndicator();
            thn_lb = th; //ganti data tahun untuk proses preroute
            m_lb = a; //ganti data bulan untuk proses preroute
            mainView.router.refreshPage();
            // picker.setValue(th+ '-' + a);
          // }
        });
        
      });
    }

    //------------ PERENCANAAN ------------//
    if(page.name === 'perencanaan'){
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel
      $$('span#nama-user').html(localStorage.nama); 
    }

    //------------ UBAH PERENCANAAN ------------//
    if(page.name === 'ubah-perencanaan'){
      $$('span#nama-user').html(localStorage.nama); 
      
      $$('#add-field').on('click', function () {
        $$("ul#mylist").append('<li class="item-divider">'+ 
                            '<i class="f7-icons color-blue" style="padding-right:5px">keyboard_fill</i>'+
                               'Data'+
                        '</li>'+
                        '<li>'+ 
                            '<div class="item-content">'+
                              '<div class="item-inner">'+
                                '<div class="item-title label">Alokasi</div> '+ 
                                '<div class="item-input">'+
                                  '<input name="nama-rencana[]" type="text" placeholder="Alokasi"'+ 
                              '</div>'+
                              '</div>'+
                            '</div>'+  
                        '</li>'+
                        '<li>'+ 
                            '<div class="item-content">'+
                              '<div class="item-inner">'+
                                '<div class="item-title label">Biaya (%)</div> '+ 
                                '<div class="item-input">'+
                                  '<input name="biaya-rencana[]" type="number" placeholder="Biaya (%)"'+ 
                              '</div>'+
                              '</div>'+
                            '</div>'+  
                        '</li>'); 
        mainView.router.refreshPreviousPage();
      });
      var x = document.querySelectorAll("input[name='biaya-rencana[]']");
      $$(x).on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('#del-field').on('click', function () {
        for(var i = 0; i < 3;i++){
         $$('ul#mylist li:last-child').remove();
        }
        mainView.router.refreshPreviousPage();
      });
      $$('input#ubah-perencanaan').on('click', function () {
        var x = document.querySelectorAll("input[name='biaya-rencana[]']");
        var jumlah = parseInt(0);  
        for (var i = 0; i < x.length; i++) {
         jumlah = jumlah + Math.abs(parseInt(x[i].value));
        }
        if(x.length == 0) modify();
        if(jumlah != 100) { myApp.alert('Jumlah biaya belum 100%','');}
        else { 
           modify();
        }
        // mainView.router.refreshPreviousPage();
      });

      function modify(){
            var i = localStorage.id_perusahaan;
            var n = document.querySelectorAll("input[name='nama-rencana[]']");
            var b = document.querySelectorAll("input[name='biaya-rencana[]']");
            var query = url_link+'perencanaan/modify';
            var data_biaya = [];
            var data_nama = [];

            for (var z = 0; z < b.length; z++) {
           data_biaya.push(Math.abs(parseInt(b[z].value)));
          }
           for (var z = 0; z < n.length; z++) {
           data_nama.push(n[z].value);
          }


            var mydata = {  id:i,
                            biaya_budget:data_biaya, 
                            nama_budget:data_nama
                          };   
            myApp.showIndicator();
            $$.post(query, mydata , function (data) {
              data = JSON.parse(data);
              myApp.hideIndicator();
              if(data.status == 'true'){
                myApp.alert('Update data berhasil.','', function () {
                mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                        force: true,
                                        ignoreCache: true
                                      });
                });
              } else
              myApp.alert('Update data gagal. Silahkan coba kembali.','');
            }); 
      }
    }

    //------------ HUTANG ------------//
    if(page.name === 'hutang'){
      $$('span#nama-user').html(localStorage.nama); 
      $$('.detail').on('click', function () {
        localStorage.id_hutang = $$(this).attr('data-index'); 
      });
    }

    //------------ CICILAN HUTANG ------------//
    if(page.name === 'cicilan-hutang'){
      $$('span#nama-user').html(localStorage.nama); 
      $$('.detail').on('click', function () {
        localStorage.id_cicilan = $$(this).attr('data-index'); 
      });
    }

    //------------ ARSIP HUTANG ------------//
    if(page.name === 'arsip-hutang'){
      $$('span#nama-user').html(localStorage.nama); 
      $$('.detail').on('click', function () {
        localStorage.id_hutang = $$(this).attr('data-index'); 
      });

      //------------ FUNGSI HAPUS DATA ------------//
      $$('.delete').on('click', function(){
        localStorage.id_hutang = $$(this).attr('data-index'); 
      });
      $$('.swipeout').on('swipeout:deleted', function(){
        var i = localStorage.id_hutang; 
        var query = url_link+'hutang/hapus';
        var mydata = {  id:i 
        };  
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
          if(data.status == 'true'){
            myApp.alert('Hapus data berhasil.','');
            mainView.router.refreshPage();
          } else
          myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
        });
      });
      //------------ END FUNGSI HAPUS DATA ------------//
    }

    //---------- PENGELUARAN ------------//
    if(page.name === 'pengeluaran'){
      var month = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var bln = month[m_klr-1];
      var a = month.indexOf(bln);
      var arr_thn = [];
      for (var i = 2016; i <= d.getFullYear(); i++) { arr_thn.push(i); }
      var thn = arr_thn.indexOf(parseInt(thn_klr));
      var target = $$(page.container).find('#picker-klr');
      var pickerPengeluaran= myApp.picker({
          input: target,
          rotateEffect: true,
          formatValue: function (p, values, displayValues) {
              return  values[0]+ '-' + values[1] ;
          },
          cols: [
              {
                  textAlign: 'left',
                  values: (function () {
                                          var arr = [];
                                          for (var i = 2016; i <= d.getFullYear(); i++) { arr.push(i); } //n = tahun sekarang
                                          return arr;
                                      })(),
              },
              { 
                  values: ('Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember').split(' '),
              },
          ],
          onOpen: function (picker) {
       
            var col0Values = picker.cols[0].values;
            var col0Random = col0Values[thn];
 
            var col1Values = picker.cols[1].values;
            var col1Random = col1Values[a];
 
            picker.setValue([col0Random, col1Random]);
        
         }
      }); 
      pickerPengeluaran.setValue([thn_klr,bln]);
      $$('input#filter-klr').on('click', function(){
        var i = localStorage.id_perusahaan;
        var month = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var dt = $$('input#picker-klr').val(); 
        var th = dt.slice(0, 4);
        var bulan = dt.slice(5);
        var a = month.indexOf(bulan)+1;
        if(a < 10) a = '0'+a;
         var mydata = {id_perusahaan:i,filter:a,tahun:th};
         console.log("bln = "+a+" thn = "+th);
         myApp.showIndicator();
        $$.post(query_pengeluaran, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON[0].status;
          // if(status === 'true') {
            myApp.hideIndicator();
            thn_klr = th; //ganti data tahun untuk proses preroute
            m_klr = a; //ganti data bulan untuk proses preroute
            mainView.router.refreshPage();
          // }
        });
        
      });
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel
      $$('span#nama-user').html(localStorage.nama); 
      $$('.edit').on('click', function () {
        localStorage.id_pengeluaran = $$(this).attr('data-index'); 
      });
      $$('.delete').on('click', function(){
        localStorage.id_pengeluaran = $$(this).attr('data-index'); 
      });
      $$('.swipeout').on('swipeout:deleted', function(){
        var i = localStorage.id_pengeluaran; 
        var query = url_link+'pengeluaran/hapus';
        var mydata = {  id:i };  
        // console.log(mydata) 
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
          if(data.status == 'true'){
            myApp.alert('Hapus data berhasil.','');
            mainView.router.refreshPage();
          } else
          myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
        });
      });
    }

    //------------ PEMASUKAN --------------//
    if(page.name === 'pemasukan'){
      var month = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
      var arr_thn = [];
      for (var i = 2016; i <= d.getFullYear(); i++) { arr_thn.push(i); }
      var thn = arr_thn.indexOf(parseInt(thn_msk));
      var bln = month[m_msk-1];
      var a = month.indexOf(bln);
      var target = $$(page.container).find('#picker-msk');
      var pickerPemasukan = myApp.picker({
          input: target,
          rotateEffect: true,
          formatValue: function (p, values, displayValues) {
              return  values[0]+ '-' + values[1] ;
          },
          cols: [
              {
                  textAlign: 'left',
                  values: (function () {
                                          var arr = [];
                                          for (var i = 2016; i <= d.getFullYear(); i++) { arr.push(i); } //n = tahun sekarang
                                          return arr;
                                      })(),
              },
              { 
                  values: ('Januari Februari Maret April Mei Juni Juli Agustus September Oktober November Desember').split(' '),
              },
          ],
          onOpen: function (picker) {
       
            var col0Values = picker.cols[0].values;
            var col0Random = col0Values[thn];
 
            var col1Values = picker.cols[1].values;
            var col1Random = col1Values[a];
 
            picker.setValue([col0Random, col1Random]);
        
         }
      }); 
      pickerPemasukan.setValue([thn_msk,bln]);
      $$('input#filter-msk').on('click', function(){
        var i = localStorage.id_perusahaan;
        var month = ['Januari','Februari','Maret','April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var dt = $$('input#picker-msk').val(); 
        var th = dt.slice(0, 4);
        var bulan = dt.slice(5);
        var a = month.indexOf(bulan)+1;
        if(a < 10) a = '0'+a;
         var mydata = {id_perusahaan:i,filter:a,tahun:th};
         myApp.showIndicator();
        $$.post(query_pemasukan, mydata , function (data) {   
          var dataJSON = JSON.parse(data);
          var status = dataJSON[0].status;
          // if(status === 'true') {
            myApp.hideIndicator();
            thn_msk = th; //ganti data tahun untuk proses preroute
            m_msk = a; //ganti data bulan untuk proses preroute
            mainView.router.refreshPage();
          // }
        });
        
      });
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel
      $$('span#nama-user').html(localStorage.nama); 
      $$('.edit').on('click', function () {
        localStorage.id_pemasukan = $$(this).attr('data-index'); 
      });
      $$('.delete').on('click', function(){
        localStorage.id_pemasukan = $$(this).attr('data-index'); 
      });
      $$('.swipeout').on('swipeout:deleted', function(){
        var i = localStorage.id_pemasukan; 
        var query = url_link+'pemasukan/hapus';
        var mydata = {  id:i };
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
          if(data.status == 'true'){
            myApp.alert('Hapus data berhasil.','');
            mainView.router.refreshPage();
          } else
          myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
        });
      });
    }

    //--------------- KATEGORI ------------------//
    if(page.name === 'kategori'){
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel
      $$('span#nama-user').html(localStorage.nama); 
      $$('.edit').on('click', function () {
        localStorage.id_kategori = $$(this).data('index'); 
        localStorage.id_relevansi = $$(this).data('relevansi'); 
      });
      $$('.delete').on('click', function(){
        localStorage.id_kategori = $$(this).attr('data-index'); 
      });
      $$('.swipeout').on('swipeout:deleted', function(){
        var i = localStorage.id_kategori; 
        var query = url_link+'kategori/hapus';
        var mydata = {  id:i 
        };  
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
          if(data.status == 'true'){
            myApp.alert('Hapus data berhasil.','');
            mainView.router.refreshPage();
          } else
          myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
        });
      });
    }

    //------------ UBAH KATEGORI -----------//
    if(page.name === 'ubah-kategori'){
      $$('span#nama-user').html(localStorage.nama);
      $$('#ubah-kategori-jenis').on('change', function(){
         if($$(this).val() === '2' ){
          $$('select#ubah-kategori-relevansi').prop('disabled', false);
        } else {
          $$('select#ubah-kategori-relevansi').prop('disabled', true);
        }
        if($$(this).val() === '1' || $$(this).val() === '2' ){
          $$('input#ubah-kategori').prop('disabled', false);
        } else {
          $$('input#ubah-kategori').prop('disabled', true);
        }
      }); 
      var i = localStorage.id_kategori;
      var ip = localStorage.id_perusahaan;
      var ir = localStorage.id_relevansi;
      var query = url_link+'kategori/getData';
      var mydata = {  id:i, id_perusahaan:ip };
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_kategori === i){
          $$('input#ubah-kategori-nama').val(data.nama);
          $$('select#ubah-kategori-warna').val(data.warna);
          $$('#after-warna').html(getWarna(data.warna));
          if(data.role != '0') {
            $$('select#ubah-kategori-jenis').val(data.role);
            $$('#ubah-kategori').prop('disabled', false);
            if(data.role === '2'){
              if(data.id_budget != null){
                $$('select#ubah-kategori-relevansi').val(data.id_budget);
              } else {
                $$('select#ubah-kategori-relevansi').val('');
              }
              $$('select#ubah-kategori-relevansi').prop('disabled', false);
            }
          }
        } else
          myApp.alert('Id Kategori tidak valid!','');
      }); 

      // event untuk ubah data
      $$('#ubah-kategori').on('click', function () { 
        var w = $$('#ubah-kategori-warna').val();
        var j = $$('#ubah-kategori-jenis').val();
        var n = $$('#ubah-kategori-nama').val();
        var r = $$('#ubah-kategori-relevansi').val();  
        var query = url_link+'kategori/ubah';
        var mydata = {  id:i,
                        id_perusahaan:ip,
                        id_relevansi:ir,
                        relevansi:r,
                        warna:w,
                        role:j, 
                        nama:n
                      };   
         console.log(mydata);
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Update data berhasil.','', function () {
            mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                    force: true,
                                    ignoreCache: true
                                  });
            });
          } else
          myApp.alert('Update data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //---------- TAMBAH KATEGORI ------------//
    if(page.name === 'tambah-kategori'){
      $$('span#nama-user').html(localStorage.nama); 
      $$('#tambah-kategori-jenis').on('change', function(){
        if($$(this).val() === '2' ){
          $$('select#tambah-kategori-relevansi').prop('disabled', false);
        } else {
          $$('select#tambah-kategori-relevansi').prop('disabled', true);
        }
        if($$(this).val() === '1' || $$(this).val() === '2' ){
          $$('input#tambah-kategori').prop('disabled', false);
        } else {
          $$('input#tambah-kategori').prop('disabled', true);
        }
      });
      // event untuk tambah data
      $$('input#tambah-kategori').on('click', function () { 
        var w = $$('#tambah-kategori-warna').val();
        var j = $$('#tambah-kategori-jenis').val();
        var n = $$('#tambah-kategori-nama').val();   
        var r = $$('#tambah-kategori-relevansi').val();   
        var mydata = {  id:localStorage.id_perusahaan,
                        warna:w,
                        role:j, 
                        relevansi:r, 
                        nama:n
                      };
        var query = url_link+'kategori/tambah';
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Tambah data berhasil.','');
            //go back and refresh its page
            mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                    force: true,
                                    ignoreCache: true
                                  });
          } else
          myApp.alert('Tambah data gagal. Silahkan coba kembali.','');
        }); 
      });
    }
    //-------- DETAIL CICILAN ---------//
    if(page.name === 'detail-cicilan'){
      $$('span#nama-user').html(localStorage.nama);
      var i = localStorage.id_cicilan;
      $$('#edit-cicilan').attr('data-index', i); //beri link edit [speed dial], id cicilan
      $$('#hapus-cicilan').attr('data-index', i); //beri link hapus [speed dial], id cicilan
      $$('#edit-cicilan').on('click', function () {
        localStorage.id_cicilan = $$(this).attr('data-index'); 
      });
      //------------ FUNGSI HAPUS DATA ------------//
      $$('#hapus-cicilan').on('click', function () {
        var i = $$(this).attr('data-index');
        var query = url_link+'hutang/hapusCicilan';
        var mydata = {  id:i };
        myApp.confirm('Apa anda yakin ingin menghapus data ini ?', 'Hapus Data', function () {
          myApp.showIndicator();
          $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
          myApp.hideIndicator();
              if(data.status == 'true'){
                myApp.alert('Hapus data berhasil.','', function () {
                mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                        force: true,
                                        ignoreCache: true
                                      });
                });
              } else
              myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
          });
        });
      });
      //------------ END FUNGSI HAPUS DATA ------------//
      //------------ FUNGSI GET DATA ------------//
      var query = url_link+'hutang/getDataCicil';
      var mydata = {  id:i  };
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_cicilan === i){
          $$('div#tanggal').html(data.tanggal_cicil);
          $$('div#kategori').html(data.kategori); 
          $$('div#mitra').html(data.partner); 
          $$('div#jumlah').html(data.jumlah);
          $$('div#keterangan').html(data.keterangan_cicil);
          $$('div#status').html(data.status);
          $$('div#sisa').html(data.sisa_hutang);
          $$('div#cicilan').html(data.jumlah_cicil);
        } else
          myApp.alert('Id Cicilan tidak valid!','');
      });  
      //------------ END FUNGSI GET DATA ------------//
    }

    //-------- UBAH CICILAN ---------//
    if(page.name === 'ubah-cicilan'){
      $$('input#ubah-cicilan-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendarDefault = myApp.calendar({  input: '#ubah-cicilan-tanggal',
                                              closeOnSelect: true,
                                            }); 
      var i = localStorage.id_cicilan;
      var query = url_link+'hutang/getDataCicil';
      var mydata = {  id:i  };
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_cicilan === i){
          $$('input#ubah-cicilan-tanggal').val(data.tanggal_cicil);
          $$('input#ubah-cicilan-jumlah').val(data.jumlah_real);
          $$('textarea#ubah-cicilan-keterangan').val(data.keterangan_cicil); 
        } else
          myApp.alert('Id cicilan tidak valid!','');
      }); 
 
      $$('input#ubah-cicilan').on('click', function () { 
        var t = $$('#ubah-cicilan-tanggal').val();
        var j = $$('#ubah-cicilan-jumlah').val();   
        var ket = $$('#ubah-cicilan-keterangan').val();
        var query = url_link+'hutang/ubahCicilan';
        var mydata = {  id:i,
                        tanggal:t,
                        jumlah:j, 
                        keterangan:ket
                      };
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Update data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 3],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
            myApp.alert('Update data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //-------- DETAIL HUTANG ---------//
    if(page.name === 'detail-hutang'){
      $$('span#nama-user').html(localStorage.nama);
      var i = localStorage.id_hutang;
      $$('#edit-hutang').attr('data-index', i); //beri link edit [speed dial], id hutang
      $$('#hapus-hutang').attr('data-index', i); //beri link hapus [speed dial], id hutang
      $$('#arsip-hutang').attr('data-index', i); //beri link hapus [speed dial], id hutang
      $$('#edit-hutang').on('click', function () {
        localStorage.id_hutang = $$(this).attr('data-index'); 
      });
      //------------ FUNGSI HAPUS DATA ------------//
      $$('#hapus-hutang').on('click', function () {
        var i = $$(this).attr('data-index');
        var query = url_link+'hutang/hapus';
        var mydata = {  id:i };
        myApp.confirm('Apa anda yakin ingin menghapus data ini ?', 'Hapus Data', function () {
          $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
              if(data.status == 'true'){
                myApp.alert('Hapus data berhasil.','', function () {
                mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                        force: true,
                                        ignoreCache: true
                                      });
                });
              } else
              myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
          });
        });
      });
      //------------ END FUNGSI HAPUS DATA ------------//
      //------------ FUNGSI ARSIP DATA ------------//
      $$('#arsip-hutang').on('click', function () {
        var i = $$(this).attr('data-index');
        var query = url_link+'hutang/arsip';
        var mydata = {  id:i };
         
          $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
              if(data.status == 'true'){
                myApp.alert('Arsip data berhasil.','', function () {
                mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                        force: true,
                                        ignoreCache: true
                                      });
                });
              } else
              myApp.alert('Arsip data gagal. Silahkan coba kembali.','');
          });
         
      });
      //------------ END FUNGSI ARSIP DATA ------------//
      //------------ FUNGSI GET DATA ------------//
      var query = url_link+'hutang/getData';
      var mydata = {  id:i  };
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_hutang === i){
          $$('div#tanggal').html(data.tanggal);
          $$('div#kategori').html(data.kategori); 
          $$('div#mitra').html(data.partner); 
          $$('div#jumlah').html(data.jumlah);
          $$('div#keterangan').html(data.keterangan);
          if(data.jatuh_tempo === '0000-00-00')
          $$('div#jatuh-tempo').html('-');
          else
          $$('div#jatuh-tempo').html(data.jatuh_tempo);
          $$('div#status').html(data.status); 
          if(data.status === 'Lunas'){
            $$('#bayar-hutang').attr('href', '#');  //hilangkan menu bayar jika status lunas
            $$('#ion-bayar').css({ opacity: .55 }); //edit css nya seperti disable
          } else {
            $$('#arsip-hutang').hide(); //hilangkan menu arsip jika status belum lunas
          }
        } else
          myApp.alert('Id Hutang tidak valid!','');
      });  
      //------------ END FUNGSI GET DATA ------------//
    }

    //---------- TAMBAH HUTANG ----------//
    if(page.name === 'tambah-hutang'){
      //antisipasi negative value
      $$('input#tambah-hutang-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      }); 
      $$('span#nama-user').html(localStorage.nama);
      var calendarDefault = myApp.calendar({  input: '#tambah-hutang-tanggal',
                                              closeOnSelect: true,
                                            }); 
      var calendar = myApp.calendar({  input: '#jatuh-tempo',
                                       closeOnSelect: true,
                                      });  
      $$('input#tambah-hutang-tanggal').val(today); // set tanggal ke hari ini
      $$('#tambah-hutang-kategori').on('change', function(){
        if($$(this).val() === '' ){
          $$('input#tambah-hutang').prop('disabled', true);
        } else {
          $$('input#tambah-hutang').prop('disabled', false);
        }
      });
      // event untuk tambah data
      $$('input#tambah-hutang').on('click', function () { 
        var t = $$('#tambah-hutang-tanggal').val();
        var k = $$('#tambah-hutang-kategori').val();
        var m = $$('#tambah-hutang-mitra').val();
        var jt = $$('#jatuh-tempo').val();
        var j = $$('#tambah-hutang-jumlah').val();   
        var ket = $$('#tambah-hutang-keterangan').val();   
        var mydata = {  id:localStorage.id_perusahaan,
                        tanggal:t,
                        jumlah:j, 
                        keterangan:ket, 
                        mitra:m, 
                        jatuh_tempo:jt, 
                        kategori:k
                      };
        var query = url_link+'hutang/tambah';
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);  
        myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Tambah data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
          myApp.alert('Tambah data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //-------- UBAH HUTANG ---------//
    if(page.name === 'ubah-hutang'){
      $$('input#ubah-hutang-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendar = myApp.calendar({ input: '#ubah-hutang-tanggal',
                                      closeOnSelect: true
                                    });
      var jatuh_tempo = myApp.calendar({  input: 'input#jatuh-tempo',
                                          closeOnSelect: true,
                                        });  
      $$('select#ubah-hutang-kategori').on('change', function(){
        if($$(this).val() === '' ){
          $$('input#ubah-hutang').prop('disabled', true);
        } else {
          $$('input#ubah-hutang').prop('disabled', false);
        }
      });
      var i = localStorage.id_hutang;
      var query = url_link+'hutang/getData';
      var mydata = {  id:i  };
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_hutang === i){
          $$('input#ubah-hutang-tanggal').val(data.tanggal);
          $$('input#ubah-hutang-jumlah').val(data.jumlah_real); 
          $$('input#ubah-hutang-mitra').val(data.partner); 
          $$('select#ubah-hutang-kategori').val(data.kategori); 
          $$('textarea#ubah-hutang-keterangan').val(data.keterangan); 
          $$('input#jatuh-tempo').val(data.jatuh_tempo); 
        } else
          myApp.alert('Id hutang tidak valid!','');
      }); 
 
      $$('#ubah-hutang').on('click', function () { 
        var t = $$('#ubah-hutang-tanggal').val();
        var k = $$('#ubah-hutang-kategori').val();
        var m = $$('#ubah-hutang-mitra').val();
        var jt = $$('input#jatuh-tempo').val();
        var j = $$('#ubah-hutang-jumlah').val();   
        var ket = $$('#ubah-hutang-keterangan').val();
        var query = url_link+'hutang/ubah';
        var mydata = {  id:i,
                        tanggal:t,
                        jumlah:j, 
                        keterangan:ket, 
                        mitra:m, 
                        jatuh_tempo:jt, 
                        kategori:k
                      };
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Update data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 3],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
            myApp.alert('Update data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //-------- BAYAR HUTANG ---------//
    if(page.name === 'bayar-hutang'){
      $$('input#bayar-hutang-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendar = myApp.calendar({ input: '#bayar-hutang-tanggal',
                                      closeOnSelect: true
                                    }); 
      $$('input#bayar-hutang-tanggal').val(today); // set tanggal ke hari ini
      var i = localStorage.id_hutang;
      var query = url_link+'hutang/getData';
      var mydata = {  id:i  };
      var sisahutang = 0;
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_hutang === i){
          sisahutang = data.jumlah_real - data.jumlahcicil;
          $$('input#bayar-hutang-tanggungan').val(data.sisahutang); 
          $$('input#bayar-hutang-mitra').val(data.partner);
        } else
          myApp.alert('Id hutang tidak valid!','');
      });
      // untuk jenis pembayaran lunas / cicil
      $$('input[type="radio"]').on('change', function () { 
        var jenis = $$(this).val();
        if(jenis === 'Lunas') $$('input#bayar-hutang-jumlah').val(sisahutang);
        else $$('input#bayar-hutang-jumlah').val('');
      });
      $$('input#bayar-hutang').on('click', function () { 
        var t = $$('#bayar-hutang-tanggal').val();
        var j = $$('input#bayar-hutang-jumlah').val();   
        var ket = $$('#bayar-hutang-keterangan').val();
        var query = url_link+'hutang/bayar';
        var mydata = {  id:i,
                        tanggal:t,
                        jumlah:j, 
                        keterangan:ket,
                        id_perusahaan:localStorage.id_perusahaan
                      };
        console.log(mydata);
        if(j > sisahutang)
          myApp.alert('Jumlah bayar melebihi sisa tanggungan. Silahkan isi kembali.','');
        else {
          myApp.showIndicator();
          $$.post(query, mydata , function (data) {  
            data = JSON.parse(data);  
            myApp.hideIndicator();
            if(data.status == 'true'){
              myApp.alert('Pembayaran berhasil.','', function () {
                mainView.router.back({  url:  mainView.history[mainView.history.length - 3],
                                        force: true,
                                        ignoreCache: true
                                      });
              });
            } else
              myApp.alert('Pembayaran gagal. Silahkan coba kembali.','');
          }); 
        }
      });
    }

    //---------- TAMBAH PENGELUARAN ----------//
    if(page.name === 'tambah-pengeluaran'){
      $$('input#tambah-pengeluaran-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendarDefault = myApp.calendar({  input: '#tambah-pengeluaran-tanggal',
                                              closeOnSelect: true,
                                            });  
      $$('input#tambah-pengeluaran-tanggal').val(today); // set tanggal ke hari ini
      $$('#tambah-pengeluaran-kategori').on('change', function(){
        if($$(this).val() === '' ){
          $$('input#tambah-pengeluaran').prop('disabled', true);
        } else {
          $$('input#tambah-pengeluaran').prop('disabled', false);
        }
      });
      // event untuk tambah data
      $$('input#tambah-pengeluaran').on('click', function () { 
        var t = $$('#tambah-pengeluaran-tanggal').val();
        var k = $$('#tambah-pengeluaran-kategori').val();
        var j = $$('#tambah-pengeluaran-jumlah').val();   
        var ket = $$('#tambah-pengeluaran-keterangan').val();   
        var mydata = {  id:localStorage.id_perusahaan,
                        tanggal:t,
                        jumlah:j, 
                        keterangan:ket, 
                        id_kategori:k
                      };
        var query = url_link+'pengeluaran/tambah';
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);  
        myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Tambah data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
          myApp.alert('Tambah data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //------- TAMBAH PEMASUKAN ---------//
    if(page.name === 'tambah-pemasukan'){
      $$('input#tambah-pemasukan-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendarDefault = myApp.calendar({  input: '#tambah-pemasukan-tanggal',
                                              closeOnSelect: true,
                                            });  
      $$('input#tambah-pemasukan-tanggal').val(today); // set tanggal ke hari ini
      $$('#tambah-pemasukan-kategori').on('change', function(){
        if($$(this).val() === '' ){
          $$('input#tambah-pemasukan').prop('disabled', true);
        } else {
          $$('input#tambah-pemasukan').prop('disabled', false);
        }
      });
      // event untuk tambah data
      $$('input#tambah-pemasukan').on('click', function () { 
        var t = $$('#tambah-pemasukan-tanggal').val();
        var k = $$('#tambah-pemasukan-kategori').val();
        var j = $$('#tambah-pemasukan-jumlah').val();   
        var ket = $$('#tambah-pemasukan-keterangan').val();   
        var mydata = {  id:localStorage.id_perusahaan,
                        tanggal:t,
                        jumlah:j, 
                        keterangan:ket, 
                        id_kategori:k
                      };
        var query = url_link+'pemasukan/tambah';
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Tambah data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
          myApp.alert('Tambah data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //-------- UBAH PEMASUKAN ---------//
    if(page.name === 'ubah-pemasukan'){
      $$('input#ubah-pemasukan-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendar = myApp.calendar({ input: '#pemasukan-tanggal',
                                      closeOnSelect: true
                                    });
      $$('select#ubah-pemasukan-kategori').on('change', function(){
        if($$(this).val() === '' ){
          $$('input#ubah-pemasukan').prop('disabled', true);
        } else {
          $$('input#ubah-pemasukan').prop('disabled', false);
        }
      });
      var i = localStorage.id_pemasukan;
      var query = url_link+'pemasukan/getData';
      var mydata = {  id:i  }; 
      var opt = $$('.smart-select option').val();
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_income === i){
          $$('input#pemasukan-tanggal').val(data.tanggal);
          $$('textarea#ubah-pemasukan-keterangan').val(data.keterangan); 
          $$('input#ubah-pemasukan-jumlah').val(data.jumlah); 
          $$('select#pemasukan-kategori').val(data.id_kategori); 
        } else
          myApp.alert('Id Pemasukan tidak valid!','');
      }); 
 
      $$('#ubah-pemasukan').on('click', function () { 
        var t = $$('#pemasukan-tanggal').val();
        var ket = $$('#ubah-pemasukan-keterangan').val();
        var j = $$('#ubah-pemasukan-jumlah').val();  
        var k = $$('#pemasukan-kategori').val();  
        var query = url_link+'pemasukan/ubah';
        var mydata = {  id:i,
                        tanggal:t,
                        jumlah:j, 
                        id_kategori:k, 
                        keterangan:ket
                      };  
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Update data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
            myApp.alert('Update data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

    //------------ UBAH PENGELUARAN ------------//
    if(page.name === 'ubah-pengeluaran'){
      $$('input#ubah-pengeluaran-jumlah').on('keydown', function (e) {  
        if(e.keyCode === 189 || e.keyCode === 190) event.preventDefault();
      });
      $$('span#nama-user').html(localStorage.nama);
      var calendar = myApp.calendar({ input: '#pengeluaran-tanggal',
                                      closeOnSelect: true
                                    });
      $$('select#ubah-pengeluaran-kategori').on('change', function(){
        if($$(this).val() === '' ){
          $$('input#ubah-pengeluaran').prop('disabled', true);
        } else {
          $$('input#ubah-pengeluaran').prop('disabled', false);
        }
      });
      var i = localStorage.id_pengeluaran;
      var query = url_link+'pengeluaran/getData';
      var mydata = {  id:i  };  
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_expense === i){
          $$('input#pengeluaran-tanggal').val(data.tanggal);
          $$('textarea#ubah-pengeluaran-keterangan').val(data.keterangan); 
          $$('input#ubah-pengeluaran-jumlah').val(data.jumlah); 
          $$('select#pengeluaran-kategori').val(data.id_kategori); 
        } else
          myApp.alert('Id Pengeluaran tidak valid!','');
      }); 
 
      $$('#ubah-pengeluaran').on('click', function () { 
        var t = $$('#pengeluaran-tanggal').val();
        var ket = $$('#ubah-pengeluaran-keterangan').val();
        var j = $$('#ubah-pengeluaran-jumlah').val();  
        var k = $$('#pengeluaran-kategori').val();  
        var query = url_link+'pengeluaran/ubah';
        var mydata = {  id:i,
                        tanggal:t,
                        jumlah:j, 
                        id_kategori:k, 
                        keterangan:ket
                      };
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Update data berhasil.','', function () {
              mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                      force: true,
                                      ignoreCache: true
                                    });
            });
          } else
            myApp.alert('Update data gagal. Silahkan coba kembali.','');
          }); 
        });
      }

    //---------- USER ------------//
    if(page.name === 'user'){
      myApp.accordionClose(".accordion-item"); //tutup accordion di panel
      $$('span#nama-user').html(localStorage.nama); 
      $$('.delete').on('click', function(){
        localStorage.id_user = $$(this).attr('data-index'); 
      });
      $$('.swipeout').on('swipeout:deleted', function(){
        var i = localStorage.id_user; 
        var query = url_link+'user/hapus';
        var mydata = {  id:i  };  
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);   
          if(data.status == 'true'){
            myApp.alert('Hapus data berhasil.','');
            mainView.router.refreshPage();
          } else
          myApp.alert('Hapus data gagal. Silahkan coba kembali.','');
        });
      });
    }

    //---------- TAMBAH USER ------------//
    if(page.name === 'tambah-user'){
      $$('span#nama-user').html(localStorage.nama); 
      $$('#tambah-role').on('change', function(){
        if($$(this).val() === '1' || $$(this).val() === '2' ){
          $$('#tambah-user').prop('disabled', false);
        } else {
          $$('#tambah-user').prop('disabled', true);
        }
      }); 

      $$('#tambah-user').on('click', function(){
        var u = $$('#tambah-username').val();
        var p = $$('#tambah-password').val();
        var r = $$('#tambah-role').val();  
        var query = url_link+'user/tambah';
        var mydata = {  id:localStorage.id_perusahaan,
                        username:u,
                        password:p, 
                        role:r
                      };
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Tambah data berhasil.','', function () {
            mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                    force: true,
                                    ignoreCache: true
                                  });
            });
          } else
          myApp.alert('Tambah data gagal. Silahkan coba kembali.','');
        });
      });
    }

    //------------ UBAH USER -----------//
    if(page.name === 'ubah-user'){
      $$('span#nama-user').html(localStorage.nama);
      $$('#ubah-role').on('change', function(){
        if($$(this).val() === '1' || $$(this).val() === '2' ){
          $$('#ubah-user').prop('disabled', false);
        } else {
          $$('#ubah-user').prop('disabled', true);
        }
      }); 
      var i = page.query.id;
      var query = url_link+'user/getData';
      var mydata = {  id:i  };
      $$.post(query, mydata , function (data) {  
        data = JSON.parse(data);   
        if(data.id_user === i){
          $$('input#ubah-username').val(data.username); 
          $$('input#ubah-password').val(data.password); 
          if(data.role != '0') {
            $$('select#ubah-role').val(data.role);
            $$('#ubah-user').prop('disabled', false);
          }
        } else
          myApp.alert('Id User tidak valid!','');
      });  

      // event untuk ubah data
      $$('#ubah-user').on('click', function () { 
        var u = $$('#ubah-username').val();
        var p = $$('#ubah-password').val();
        var r = $$('#ubah-role').val();  
        var query = url_link+'user/ubah';
        var mydata = {  id:i,
                        username:u,
                        password:p, 
                        role:r
                      };  
        myApp.showIndicator();
        $$.post(query, mydata , function (data) {  
          data = JSON.parse(data);  
          myApp.hideIndicator();
          if(data.status == 'true'){
            myApp.alert('Ubah data berhasil.','', function () {
            mainView.router.back({  url:  mainView.history[mainView.history.length - 2],
                                    force: true,
                                    ignoreCache: true
                                  });
            });
          } else
            myApp.alert('Update data gagal. Silahkan coba kembali.','');
        }); 
      });
    }

}); // END On-Page INIT

myApp.init(); 